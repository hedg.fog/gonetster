package neterr

const (
	ErrExit = NetErr("exit")
)

type NetErr string

func (e NetErr) Error() string {
	return string(e)
}
