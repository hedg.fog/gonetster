package gui

type GUI interface {
	Init() error
	Run() error
}
