package console

import (
	"github.com/gdamore/tcell/v2"
	"gonetster/config"
	"gonetster/gui/console/codes"
	"gonetster/gui/console/widget"
)

func (c *Console) Layout() {
	c.Tooltip = widget.NewText(widget.Style{
		DefStyle: tcell.StyleDefault.Foreground(tcell.ColorWhite),
	}, "Hello World")

	menu := widget.NewMenu(widget.Style{
		X:        1,
		Y:        1,
		Width:    20,
		DefStyle: tcell.StyleDefault.Foreground(tcell.ColorWhite),
		SelStyle: tcell.StyleDefault.Foreground(tcell.ColorWhite).Background(tcell.ColorGray),
	}, []string{"Call Method", "Settings"}, []widget.ActionFunc{c.CallMethod, c.CallMethod})

	menu.SetSelect(true)

	c.widgets = append(c.widgets, menu)
}

func (c *Console) lastX() int {
	return c.widgets[len(c.widgets)-1].GetStyle().X + c.widgets[len(c.widgets)-1].GetStyle().Width + 1
}

func (c *Console) CallMethod() codes.ActionCode {
	widgets := make([]widget.Widget, len(c.Config.Methods))

	x := c.lastX()
	y := 1

	for i, method := range c.Config.Methods {
		y++

		widgets[i] = widget.NewButton(widget.Style{
			X:        x + 1,
			Y:        y,
			DefStyle: tcell.StyleDefault.Foreground(tcell.ColorWhite),
			SelStyle: tcell.StyleDefault.Foreground(tcell.ColorWhite).Background(tcell.ColorGray),
			Action:   c.MethodView(method),
		}, method.Name)
	}

	widgets[0].SetSelect(true)

	panel := widget.NewPanel(widget.Style{
		X:     x,
		Y:     1,
		Width: 20,
	}, widgets)

	c.widgets = append(c.widgets, panel)

	panel.SetSelect(true)
	c.getSelect().SetSelect(false)

	return codes.OK
}

func (c *Console) MethodView(method config.Method) widget.ActionFunc {
	return func() codes.ActionCode {
		widgets := make([]widget.Widget, len(method.Fields))

		x := c.lastX()
		y := 1

		for i, field := range method.Fields {
			y++

			widgets[i] = widget.NewEditText(widget.Style{
				X:        x + 1,
				Y:        y,
				DefStyle: tcell.StyleDefault.Foreground(tcell.ColorWhite),
				SelStyle: tcell.StyleDefault.Foreground(tcell.ColorWhite).Background(tcell.ColorGray),
				Action: func() codes.ActionCode {
					c.Tooltip.Text = config.Tooltip(field.Type)
					return codes.OK
				},
			}, field.Name+": ", field.Type)
		}

		widgets[0].SetSelect(true)

		panel := widget.NewPanel(widget.Style{
			X:     x,
			Y:     1,
			Width: 20,
		}, widgets)

		c.widgets = append(c.widgets, panel)

		panel.SetSelect(true)
		c.getSelect().SetSelect(false)

		return codes.OK
	}
}
