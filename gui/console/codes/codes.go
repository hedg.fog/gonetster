package codes

type ActionCode int

const (
	OK                ActionCode = iota
	NonWidgetCode     ActionCode = iota
	EmptyList         ActionCode = iota
	UpdateTooltip     ActionCode = iota
	IncorrectTextType ActionCode = iota
)
