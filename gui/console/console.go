package console

import (
	"fmt"
	"gonetster/config"
	"gonetster/gui/console/widget"
	"gonetster/neterr"

	"github.com/gdamore/tcell/v2"
	"github.com/gdamore/tcell/v2/encoding"
)

type Console struct {
	s tcell.Screen

	Tooltip *widget.Text
	Config  *config.Config
	widgets []widget.Widget
}

func NewConsole() *Console {
	return &Console{}
}

func (c *Console) Init() error {
	encoding.Register()

	screen, err := tcell.NewScreen()
	if err != nil {
		return fmt.Errorf("failed to create termui: %w", err)
	}

	if err := screen.Init(); err != nil {
		return fmt.Errorf("failed to initialize termui: %w", err)
	}

	c.s = screen

	return nil
}

func (c *Console) Run() error {
	c.Layout()

	c.Draw()

	for {
		err := c.Action(c.s.PollEvent())
		if err != nil {
			return err
		}
	}
}

func (c *Console) Draw() {
	c.Tooltip.Draw(c.s)

	for _, w := range c.widgets {
		w.Draw(c.s)
	}
}

func (c *Console) ActionDraw() {
	c.s.Sync()
	c.s.Clear()

	c.Draw()

	c.s.Show()
}

func (c *Console) Action(event tcell.Event) error {
	switch ev := event.(type) {
	case *tcell.EventResize:
		c.ActionDraw()
	case *tcell.EventKey:
		c.getSelect().Action(ev)

		if ev.Key() == tcell.KeyEscape {
			return neterr.ErrExit
		}

		c.ActionDraw()
	}

	return nil
}

func (c *Console) Close() {
	c.s.Fini()
}

func (c *Console) getSelect() widget.Widget {
	for _, w := range c.widgets {
		if w.IsSelect() {
			return w
		}
	}

	return nil
}
