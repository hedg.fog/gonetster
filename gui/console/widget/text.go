package widget

import "github.com/gdamore/tcell/v2"

type Text struct {
	Style

	Text string
}

func NewText(style Style, text string) *Text {
	return &Text{
		Style: style,
		Text:  text,
	}
}

func (t *Text) Draw(s tcell.Screen) {
	if t.IsSelect() {
		drawText(s, t.Style.SelStyle, t.X, t.Y, t.Text)
	} else {
		drawText(s, t.Style.DefStyle, t.X, t.Y, t.Text)
	}
}
