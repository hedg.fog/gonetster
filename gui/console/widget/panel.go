package widget

import (
	"github.com/gdamore/tcell/v2"
	"gonetster/gui/console/codes"
)

type Panel struct {
	Style

	widgets []Widget
}

func NewPanel(style Style, widgets []Widget) *Panel {
	return &Panel{
		Style:   style,
		widgets: widgets,
	}
}

func (p *Panel) Draw(s tcell.Screen) {
	_, maxY := s.Size()

	drawBox(s, p.Style.DefStyle, p.X, p.Y, p.Width, maxY-1-p.Y) //todo change 20 to max item length plus delta

	for _, widget := range p.widgets {
		widget.Draw(s)
	}
}

func (p *Panel) Action(ev *tcell.EventKey) codes.ActionCode {
	if len(p.widgets) == 0 {
		return codes.EmptyList
	}

	index := p.getSelect()

	switch ev.Key() {
	case tcell.KeyDown:
		p.widgets[index].SetSelect(false)
		p.widgets[(index+1)%len(p.widgets)].SetSelect(true)
	case tcell.KeyUp:
		p.widgets[index].SetSelect(false)
		if index-1 < 0 {
			index = len(p.widgets) - 1
		} else {
			index--
		}

		p.widgets[index].SetSelect(true)

		return codes.OK
	case tcell.KeyEnter:
		return p.widgets[index].Action(ev)
	case tcell.KeyRune:
		return p.widgets[index].Action(ev)
	default:

		return codes.NonWidgetCode
	}

	return 0
}

func (p *Panel) getSelect() int {
	for i := range p.widgets {
		if p.widgets[i].IsSelect() {
			return i
		}
	}

	return -1
}
