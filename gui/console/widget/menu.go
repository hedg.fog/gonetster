package widget

import (
	"github.com/gdamore/tcell/v2"
	"gonetster/gui/console/codes"
)

type Menu struct {
	Style

	items []*Button
}

func NewMenu(style Style, items []string, functions []ActionFunc) *Menu {
	buttons := make([]*Button, len(items))
	y := style.Y

	for i := range items {
		y++

		btnStyle := style
		btnStyle.X = style.X + 1
		btnStyle.Y = y
		btnStyle.Action = functions[i]

		buttons[i] = NewButton(btnStyle, items[i])
	}

	if len(buttons) > 0 {
		buttons[0].SetSelect(true)
	}

	return &Menu{
		items: buttons,
		Style: style,
	}
}

func (m *Menu) Draw(s tcell.Screen) {
	_, maxY := s.Size()

	drawBox(s, m.Style.DefStyle, m.X, m.Y, m.Width, maxY-1-m.Y) //todo change 20 to max item length plus delta

	for _, item := range m.items {
		item.Draw(s)
	}
}

func (m *Menu) Action(ev *tcell.EventKey) codes.ActionCode {
	if len(m.items) == 0 {
		return codes.EmptyList
	}

	index := m.getSelect()

	switch ev.Key() {
	case tcell.KeyDown:
		m.items[index].SetSelect(false)
		m.items[(index+1)%len(m.items)].SetSelect(true)
	case tcell.KeyUp:
		m.items[index].SetSelect(false)
		if index-1 < 0 {
			index = len(m.items) - 1
		} else {
			index--
		}

		m.items[index].SetSelect(true)

		return codes.OK
	case tcell.KeyEnter:
		return m.items[index].Action(ev)
	default:
		return codes.NonWidgetCode
	}

	return 0
}

func (m *Menu) getSelect() int {
	for i := range m.items {
		if m.items[i].IsSelect() {
			return i
		}
	}

	return -1
}
