package widget

import (
	"github.com/gdamore/tcell/v2"
	"gonetster/gui/console/codes"
)

type Widget interface {
	Draw(screen tcell.Screen)
	Action(ev *tcell.EventKey) codes.ActionCode
	Selection()
	IsSelect() bool
	SetSelect(sel bool)
	GetStyle() *Style
}
