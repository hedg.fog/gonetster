package widget

import (
	"github.com/gdamore/tcell/v2"
	"gonetster/config"
	"gonetster/gui/console/codes"
	"log"
)

type EditText struct {
	Style
	Text

	Type config.TextType
}

func NewEditText(style Style, text string, textType config.TextType) *EditText {
	return &EditText{
		Style: style,
		Type:  textType,
		Text: Text{
			Style: style,
			Text:  text,
		},
	}
}

func (e *EditText) Draw(s tcell.Screen) {
	e.Text.Draw(s)
}

func (e *EditText) Action(ev *tcell.EventKey) codes.ActionCode {
	if !e.Active {
		e.Active = true

		return e.Style.Action()
	}

	out, _, err := config.Event(e.Type, ev)
	if err != nil {
		log.Fatal(err) //todo logging this
	}

	e.Text.Text += out

	return codes.IncorrectTextType
}

func (e *EditText) SetSelect(sel bool) {
	e.Select = sel
	e.Text.SetSelect(sel)
}
