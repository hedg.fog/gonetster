package widget

import (
	"github.com/gdamore/tcell/v2"
	"gonetster/gui/console/codes"
)

type ActionFunc func() codes.ActionCode
type SelectionFunc func()

type Style struct {
	X, Y          int
	Width, Height int

	Select bool
	Active bool

	DefStyle tcell.Style
	SelStyle tcell.Style

	Action        ActionFunc
	SelectionFunc SelectionFunc
}

func (s *Style) IsSelect() bool {
	return s.Select
}

func (s *Style) SetSelect(sel bool) {
	s.Select = sel
}

func (s *Style) GetStyle() *Style {
	return s
}

func (s *Style) Selection() {
	if s.SelectionFunc != nil {
		s.SelectionFunc()
	}
}

// todo add option style
