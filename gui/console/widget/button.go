package widget

import (
	"github.com/gdamore/tcell/v2"
	"gonetster/gui/console/codes"
)

type Button struct {
	Style
	Text
}

func NewButton(style Style, text string) *Button {
	return &Button{
		Style: style,
		Text: Text{
			Style: style,
			Text:  text,
		},
	}
}

func (b *Button) Draw(s tcell.Screen) {
	b.Text.Draw(s)
}

func (b *Button) Action(ev *tcell.EventKey) codes.ActionCode {
	return b.Style.Action()
}

func (b *Button) SetSelect(sel bool) {
	b.Select = sel
	b.Text.SetSelect(sel)
}
