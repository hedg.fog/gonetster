package config

type Method struct {
	Name   string
	Fields []Field
}
