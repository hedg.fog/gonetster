package config

import (
	"github.com/gdamore/tcell/v2"
	"github.com/google/uuid"
	"gonetster/neterr"
)

const (
	ErrUndefined = neterr.NetErr("undefined type")
)

type Field struct {
	Name string
	Type TextType
}

type TextType string

const (
	UUIDBytes TextType = "uuid.bytes"
)

func Tooltip(textType TextType) string {
	switch textType {
	case UUIDBytes:
		return "Google UUID in bytes format: 1 - uuid.New()"
	}

	return "UNDEFINED"
}

func Event(textType TextType, ev *tcell.EventKey) (out string, val interface{}, err error) {
	switch textType {
	case UUIDBytes:
		if ev.Rune() == '1' {
			id := uuid.New()

			idBytes, err := id.MarshalBinary()
			if err != nil {
				return "", nil, err
			}

			return "uuid.New()", idBytes, nil
		}
	}

	return "UNDEFINED", nil, ErrUndefined
}
