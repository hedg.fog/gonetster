package config

type Config struct {
	Methods []Method
}

func NewConfig(methods []Method) *Config {
	return &Config{
		Methods: methods,
	}
}
