package main

import (
	"errors"
	"gonetster/config"
	"gonetster/gui/console"
	"gonetster/neterr"
	"log"
)

func main() {
	c := console.NewConsole()
	c.Config = &config.Config{
		Methods: []config.Method{
			{
				Name: "CreateAccount",
				Fields: []config.Field{
					{
						Name: "RequestID",
						Type: config.UUIDBytes,
					},
				},
			},
			{
				Name: "SendRawTransaction",
			},
		},
	}
	if err := c.Init(); err != nil {
		log.Fatal(err)
	}

	if err := c.Run(); err != nil && !errors.Is(err, neterr.ErrExit) {
		log.Fatal(err)
	}

	c.Close()
}
